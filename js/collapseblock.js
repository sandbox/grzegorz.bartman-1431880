Drupal.behaviors.collapseBlock = function (context) {
  var blocks= Drupal.settings.collapse_block.blocks;
  var active_url= Drupal.settings.collapse_block.active_url;
    $(blocks).each(function(index) {      
      var id = '#block-'+blocks[index]+' .menu li a';
      $($(id).get()).each(function(index2) {
        var link = $(id).get(index2);
        var href = $(link).attr("href");
        if(href==active_url){
          var content = '#block-'+blocks[index]+' .content';
          $(content).show()
        }
      });     
    });
};
