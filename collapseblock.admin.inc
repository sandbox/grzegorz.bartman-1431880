<?php

/**
 * @file
 * Settings.
 */

/**
 * Settings form.
 *
 * @return
 *   Form array.
 */
function collapseblock_settings() {
  $blocks = _block_rehash();
  $options = array();
  foreach ($blocks as $block) {
    $options[$block['module'] . '-' . $block['delta']] =
            '[' . $block['module'] . '-' . $block['delta'] . '] ' . $block['info'];
  }
  $form = array();
  $form['collapseblock_blocks_list'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Blocks'),
      '#required' => TRUE,
      '#options' => $options,
      '#default_value' => variable_get('collapseblock_blocks_list', array()),
  );
  return system_settings_form($form);
}