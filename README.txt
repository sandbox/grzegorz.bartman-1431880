Allow to collapse drupal blocks.

Configuration:
----------------------
Go to dmin/settings/collapseblock and select blocks.


Authors:
----------------------
openBIT http://www.openbit.pl
http://drupal.org/user/1642904 Piotr Łoposzko
http://drupal.org/user/363120 Grzegorz Bartman